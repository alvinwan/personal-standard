from flask import Blueprint, request
from flask_login import login_required
from app.views import permission_required, render
from .utils import Log
from app.utils import Search

# setup Blueprint
log = Blueprint('log', __name__, url_prefix='/admin/log')


@log.route('/')
@login_required
@permission_required('log_view')
def home():
	logs = Search(request.args.get('query', ''), Log.model).query()
	return render('search.html', mod='log', logs=logs)


@log.route('/<string:logId>')
@login_required
@permission_required('log_view')
def detail(logId):
	log = Log(id=logId).get()
	return render('detail.html', mod='log', log=log)