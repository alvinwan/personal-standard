from flask import Blueprint, render_template

# setup Blueprint
admin = Blueprint('admin', __name__, url_prefix='/admin')

@admin.route('/')
def home():
	return render_template('admin/home.html')