from app.models import Document, db
from app.auth.models import User


class Notification(Document):
	message = db.StringField()  # markdown-flavored
	category = db.IntField()


class Log(Document):
	object = db.StringField()
	user = db.ReferenceField(User)
	action = db.StringField()
	category = db.StringField()
	oid = db.StringField()
	detail = db.DictField()