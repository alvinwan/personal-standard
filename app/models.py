from flask.ext.mongoengine import MongoEngine
import datetime

db = MongoEngine()


class Document(db.Document):
	created_at = db.DateTimeField(default=datetime.datetime.now)
	updated_at = db.DateTimeField(default=datetime.datetime.now)

	meta = {
		'abstract': True
	}