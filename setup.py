from app.auth.utils import Role, User
from mongoengine.errors import DoesNotExist
import config

roles = config.roles

def build(args):
	global roles
	for role in roles:
		Role(name=role[0]).load(permissions=role[1]).save()
	print('Build complete.')

		
def grant(args):
	try:
		user = User(email=args.email).get()
		role = Role(name=args.role).get()
		user.load(role=role).save()
	except DoesNotExist as e:
		print(e)

import argparse

parser = argparse.ArgumentParser(description='Setup Puhjee with initial database settings')
parser.add_argument('command', choices=['build', 'grant'],
                    help='build the default database')
parser.add_argument('-e', '--email', type=str, help='specify user email')
parser.add_argument('-r', '--role', type=str, help='specify name of role')

args = parser.parse_args()
globals()[args.command](args)