from flask import Flask
from flask_mongoengine import MongoEngine
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_kvsession import KVSessionExtension
from simplekv.db.mongo import MongoStore
from simplekv import KeyValueStore

import config

# Create and name app
app = Flask(__name__,
            static_folder=config.STATIC_PATH)

# database connection
app.config['MONGODB_SETTINGS'] = {'DB': config.DB}
app.config['SECRET_KEY'] = config.SECRET_KEY
app.debug = config.DEBUG
app.secret_key = config.SECRET_KEY

# initialize MongoEngine with app
db = MongoEngine()
db.init_app(app)
store = MongoStore(
    getattr(db.connection, config.DB),
    config.SESSION_STORE)
session = KeyValueStore()

# Substitute client-side with server-side sessions
kv_session = KVSessionExtension()
kv_session.init_app(app, store)

# initialize Flask-Login with app
login_manager = LoginManager()
login_manager.init_app(app)

# initialize encryption mechanism
bcrypt = Bcrypt()
bcrypt.init_app(app)

from app.public.views import public
from app.api.views import api
from app.auth.views import auth
from app.log.views import log
from app.admin.views import admin

app.register_blueprint(public)
app.register_blueprint(api)
app.register_blueprint(auth)
app.register_blueprint(log)
app.register_blueprint(admin)