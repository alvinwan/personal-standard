from bson import ObjectId
from flask import render_template, request, Blueprint

# setup Blueprint
api = Blueprint('api', __name__, url_prefix='/api')