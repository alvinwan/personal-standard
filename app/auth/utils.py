from app.utils import Parent
from . import models
import config

from flask_login import UserMixin
from app import store


class User(UserMixin, Parent):
	"""
	System User
	"""

	model = models.User

	def is_active(self):
		"""
		If the user is considered 'active' in application
		:return: boolean
		"""
		return not getattr(self, 'is_suspended', None)

	def is_authenticated(self):
		"""
		Method of checking authentication
		:return: boolean
		"""
		return self.exists()

	def is_anonymous(self):
		"""
		If the user is not logged in
		:return: boolean
		"""
		return not self.exists()

	def get_id(self):
		"""
		ID for the current user object
		:return: str or None
		"""
		return str(self.id) if self.exists() else None


class Role(Parent):
	"""
	Roles for each user
	"""

	model = models.Role


class Allow:
	"""
	Static methods for authorization
	"""

	@staticmethod
	def ed(user, permissions):
		"""
		Handles permissions, checks if user's role has said permission
		:param user: current_user
		:param permission: the string permission name to check for, or a list of all requirements
		:return: boolean
		"""
		if not user or not user.is_authenticated() or not hasattr(user, 'role'):
			return False
		role = Role(id=user.role).get()
		if role.name == 'admin':
			return True
		if isinstance(permissions, list):
			return len(
				list(set(permissions) & set(role.permissions))
			) == len(permissions)
		return permissions in getattr(role, 'permissions', ())


class Alert:

	def __init__(self, message, class_='notokay', **kwargs):
		self.message = message
		self.class_ = class_

	@staticmethod
	def check():
		"""
		Checks for alerts in session
		:return: alert or None
		"""
		if 'alert' in store:
			data = store.get('alert')
			store.delete('alert')
			return Alert(**data)

	def log(self):
		"""
		Save alert in session.
		:return: self
		"""
		store.put('alert', {
			'message': self.message,
			'class_': self.class_
		})
		return self

	def __str__(self):
		"""
		Returns the message
		:return: str
		"""
		return self.message