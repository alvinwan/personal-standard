from app import db
from app.models import Document


class Role(Document):
	name = db.StringField()
	permissions = db.ListField(default=())
	
	def __str__(self):
		return self.name


class User(Document):
	name = db.StringField()
	address = db.StringField()
	type = db.IntField()  # customer, agent, office
	email = db.StringField(unique=True)
	password = db.StringField()  # hashed using sha256
	status = db.IntField()  #
	role = db.ReferenceField(Role, dbref=False)
	
	def __str__(self):
		return self.name+': '+self.email