from bson import ObjectId
from flask import render_template, request, Blueprint

# setup Blueprint
public = Blueprint('public', __name__)

@public.route('/')
def index():
	return render_template('auth/index.html')