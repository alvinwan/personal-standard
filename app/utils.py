import datetime
from flask_login import current_user
from bson.dbref import DBRef
from bson import ObjectId
from app import db
from app.auth.models import User


class Parent:
	"""
	This is the base class for all savable objects.
	"""

	filters = {}

	def __init__(self, **kwargs):
		"""
		Automatically loads all kwargs as filters
		:param kwargs: kwargs
		:return: self
		"""
		self.filter(**kwargs)
		self.result = None  # for query results

	def get(self):
		"""
		Query the database with this object's data
		:return: object loaded with data
		"""
		self.result = self.__class__._get(**self.data())
		data = Parent._data(self, self.result)
		return self.load(**data)

	@classmethod
	def _get(cls, **kwargs):
		"""
		Query database with passed-in data
		:param kwargs: filter
		:return: query results or None
		"""
		return cls.model.objects(**kwargs).get()

	def save(self):
		"""
		Save object with it's data to database
		:return: self
		"""
		if len(list(self.filters.keys())) == 0:
			self.filters = self.data()
		data = {'set__%s' % k: v for k, v in self.data(defaults=True).items()}
		self.result = self.model.objects(**self.filters).modify(
			upsert=True,
			new=True,
			**data
		)
		action = 'UPDATE' if hasattr(self, 'id') else 'INSERT'
		data = Parent._data(self, self.result)
		self.load(**data)
		self.log(action=action, category='DATABASE', oid=self.ID, detail=data)
		return self

	def load(self, idify=False, **kwargs):
		"""
		Save all passed-in data to object
		- convert all ID objects and strings into ObjectIds
		:param kwargs: data
		:return: self
		"""
		for k, v in kwargs.items():
			if k == 'id' or (k in self.model._fields.keys() \
					and isinstance(self.model._fields[k], db.ReferenceField)):
				if idify or k == 'id':
					if not isinstance(v, ObjectId):
						if isinstance(v, str):
							v = ObjectId(v)
						else:
							v = getattr(v, 'id')
				elif issubclass(v.__class__, db.Document):
					v = v.to_dbref()
				elif isinstance(v, str):
					v = DBRef(k, ObjectId(v))
				elif hasattr(v, 'model'):
					v = v.model(**v.data()).to_dbref()
				else:
					v = None
			setattr(self, k, v)
		return self

	def data(self, defaults=False):
		"""
		Returns all data associated with object
		:return: data for specified fields, or all data
		"""
		return Parent._data(self, self, defaults)

	@staticmethod
	def _data(self, obj, defaults=False):
		data = {}
		for f, v in self.fields().items():
			if f.startswith('updated_'):
				setattr(obj, f, None)
			if getattr(obj, f, None):
				data[f] = getattr(obj, f)
			elif getattr(v, 'default', None) and defaults:
				data[f] = v.default()
		return data

	def fields(self):
		"""
		Fetches all fields based on this object's model
		:return: list of fields
		"""
		return Parent._fields(getattr(self, 'model'))

	@staticmethod
	def _fields(model):
		"""
		Fetches all fields for a model
		:param model: model
		:return: dictionary of fields
		"""
		return {k: v for k, v in model._fields.items() if not isinstance(k, int)}

	def __str__(self):
		"""
		Pretty string representation
		:return: string
		"""
		return '{class_}({data})'.format(
			class_=self.__class__.__name__,
			data=','.join([k+'='+str(v) for k, v in self.data().items()])
		)

	def filter(self, **kwargs):
		"""
		Set filters for next save or search.
		:param kwargs: filters as kwargs
		:return: self
		"""
		self.load(**kwargs)
		self.filters = dict(self.data())
		return self

	def delete(self):
		"""
		Delete the object
		:return: None
		"""
		self.log(action='DELETE', category='DATABASE', oid=self.ID, detail=self.data())
		self.get().model(**self.data()).delete()

	def assemble(self, form):
		"""
		Assemble all data from form and package into info.
		:return: self
		"""
		self.info = {k: v.data for k, v in form._fields.items()}
		return self

	def exists(self):
		"""
		Test if the current object exists.
		:return:
		"""
		if not self.result:
			self.get()
		return hasattr(self, 'id')

	def load_multidict(self, **kwargs):
		""" 
		Returns object of this class type using multidict
		:param kwargs: fields
		:return: obj
		"""
		extract = lambda lst: lst[0] if len(lst) == 1 else lst
		kwargs = {k: extract(v) for k, v in kwargs.items()}
		return self.load(**kwargs)
	
	@property
	def ID(self):
		return str(self.id)

	def log(self, **kwargs):
		"""
		Log action 
		"""
		from app.log.utils import Log
		obj = self.__class__.__name__
		if obj.lower() != 'log':
			kwargs['object'] = obj
			kwargs['user'] = current_user
			return Log().load(**kwargs).save()

import re


class Search:
	"""
	Handles the search utility
	"""
	
	defaults = {}

	# all allowed operators: http://docs.mongoengine.org/guide/querying.html
	operators = {
		'eq': '',
		'equal': '',
		'before': '__lt',
		'after': '__gt',
	    'not_in': '__nin',
	    'neq': '__ne'
	}
	
	def __init__(self, query, model):
		self.query = self.querify(query, model)

	def query(self, page=1, num_per_page=20):
		start, end = Search.limits(page, num_per_page)
		return self.query.all()[start:end]

	@staticmethod
	def tokenize(query):
		"""
		Parses each command for flag, op, and arg
		Regex captures first named group "flag" as a string preceded
		by a single dash, followed by a space. Optionally captures
		second named group "op" as a string preceded by two dashes
		and followed by a space. Captures final named group "arg"
		with optional quotations.
		"""
		tokenizer = re.compile('-(?P<flag>[\S]+)\s+(--(?P<op>[\S]+)\s+)?"?(?P<arg>[\S ]{0,}[^"\s]+)"?')
		return tokenizer.findall(query)
	
	@classmethod
	def translate(cls, query):
		""" converts operators into appropriate string reps and adds defaults """
		tokens = cls.tokenize(query)
		scope = {k: tuple(v) for k, v in cls.defaults.items()}
		for token in tokens:
			flag, dummy, opr, arg = token
			scope[flag] = (cls.operators.get(opr or 'eq', '__'+opr), arg)
		return scope
	
	@classmethod
	def objectify(cls, query, model):
		""" converts keys into objects - doesn't do anything... yet"""
		scope = cls.translate(query)
		for k, v in scope.items():
			op, arg = v
			scope[k] = (op, arg)
		return scope
	
	@classmethod
	def querify(cls, query, model):
		""" converts mush into a query object """
		objects = cls.objectify(query, model)
		kwargs = cls.get_args(model, objects)
		query = model.objects(**kwargs).all()
		return query
	
	@staticmethod
	def get_args(model, prime):
		""" Creates all filters """
		return {k+v[0]: v[1] for k, v in prime.items()}
	
	@staticmethod
	def limits(page, num_per_page):
		""" returns start and ends number based on page and num_per_page """
		return (page-1)*num_per_page, page*num_per_page