import os

get = os.environ.get

# application setup and security
PORT = get('PORT', 8000)
HOST = get('HOST', '127.0.0.1')
SCHEME = get('SCHEME', 'http')
HASH_ROUNDS = 2

# database connection
SECRET_KEY = get('SECRET_KEY', 'flask+mongoengine=<3')
DB = get('DB', 'tracker')
SESSION_STORE = 'session'

# testing
DEBUG = get('DEBUG', True)

# define application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
STATIC_PATH = 'static'

# URL
BASE_URL = 'http://52.2.207.224'

# default roles - the name "admin" gives rights to everything
roles = (
	('admin', ()),
	('salesperson', ()),
	('agent', ()),
	('user', ())
)