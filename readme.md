#Trends Project : Consumer Relations Management
*Part D : a web application to facilitate use of the Project*

This web application is split according to N-tier application standards. Here, you will find the following:

1. Public -- this houses all templates and front-end renders, interacts only with the API
2. API -- this is responsible for business logic and connections to the datastore

##Installation

**Short Version**

1. Run `check.sh` to ensure both `python3` and `mongodb` are installed.
2. Run `install.sh`.

Installation is complete. If installation fails, see the long version below to debug step-by-step.

**Long Version**

1. Ensure Python3 and its virtualenv module are installed.
2. Start a virtualenv `python3 -m venv env`.
3. Create the mongodb datastore directory `mkdir env/db`.
4. Launch the virtualenv `source env/bin/activate`.
5. Install all requirements `pip3 install -r requirements.txt`.
6. Run the mongodb server `mongodb --dbpath env/db`.

##Testing
To run tests, use `py.test tests`.

##Getting Started

1. Launch server `source activate.sh`.